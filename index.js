/* global __dirname */

const express = require('express');
const path = require('path');
const axios = require('axios');
const sharp = require('sharp')
const slugify = require('slugify');
const download = require('image-downloader');
const app = module.exports = express();
const { access, constants, unlink, copyFile } = require("fs");


// /files/* is accessed via req.params[0]
// but here we name it :file
app.get('/:file(*)', function (req, res, next) {

    if (!req.params.file.endsWith('.png')) {
        res.statusCode = 404;
        res.send('That is not a valid file extension!');
        return;
    }

    const sanitizedFileName = slugify(
        path.parse(req.params.file).name,
        { locale: 'no', replacement: '_', lower: true, strict: true }
    ) + path.extname(req.params.file);
    var filePath = path.join(__dirname, 'images', sanitizedFileName);
    var fileName = path.parse(sanitizedFileName).name;

    const returnFile = () => {
        res.sendFile(filePath, function (err) {
            if (!err) return; // file sent
            if (err.status !== 404) return next(err); // non-404 error
            // file for download not found
            res.statusCode = 404;
            res.send('Cant find that file, sorry!');
        });
    }

    const googleImageSearch = (searchTerm) => {
        const path = 'https://customsearch.googleapis.com/customsearch/v1';
        return axios.get(path, {
            params: {
                key: 'AIzaSyC90kHx5YphrhMYmTbDl4oNdwECWDT_GEc',
                cx: 'fc0e4747696039cb3',
                gl: 'no',
                imgSize: 'large',
                num: '10',
                safe: 'active',
                searchType: 'IMAGE',
                q: searchTerm,
            }
        });
    }

    // Searches google for an image and returns the URL(s)
    const searchImage = async (searchTerm) => {
        console.log("Search term: " + searchTerm);

        const ans = await googleImageSearch(searchTerm);

        const urls = ans.data.items ? ans.data.items.map(i => i.link) : null;
        if (urls) {
            return urls;
        }

        if (ans.data.spelling) {
            const lastTry = await googleImageSearch(ans.data.spelling.correctedQuery);
            const urls = lastTry.data.items ? lastTry.data.items.map(i => i.link) : null;
            if (urls) {
                return urls;
            }
        }

        return [];
    }

    // Downloads an image into the downloads folder and returns the path
    const downloadImage = async (url, dest) => {
        return download.image({
            url,
            dest,
        });
    }

    // Converts an image into a 400x400 PNG image named newPath
    const convert = (path, newPath) => {
        return new Promise((resolve, reject) => {
            sharp(path)
                .resize({ height: 400, width: 400, fit: 'cover' })
                .toFile(newPath, (err, info) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(info);
                    }
                })
        })
    }

    // Deletes a file from the filesystem
    const deleteFile = (path) => {
        return new Promise((resolve, reject) => unlink(path, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        }));
    };
    access(filePath, constants.F_OK, async (err) => {
        console.log("Got request");
        if (err) {
            try {
                console.log("File doesn't exist");
                // File does not exist, search and download
                const imageUrls = await searchImage(path.parse(req.params.file).name.replace(/[-_]/g, ' '));
                let success = false;
                for (let i = 0; i < imageUrls.length && !success; i++) {
                    const url = imageUrls[i];
                    try {
                        console.log("Attempting download: " + url);
                        const tempPath = path.join(__dirname, 'temp', `${fileName}${path.extname(url)}`);
                        await downloadImage(url, tempPath);
                        await convert(tempPath, filePath);
                        await deleteFile(tempPath);
                        console.log("Download successful");
                        success = true;
                    } catch (e) {
                        console.error(e);
                    }
                }
                if (!success) {
                    try {
                        copyFile('defaultImage.png', filePath, (err) => {
                            if (err) throw err;
                        });
                        success = true;
                    } catch (e) {
                        res.statusCode = 404;
                        res.send('Could not fetch images');
                        return;
                    }
                }
            } catch (e) {
                res.statusCode = 404;
                res.send('Something went wrong');
                return;
            }
        }
        // File should now exist, return it
        console.log("Returning image file");
        returnFile();
    });
});

if (!module.parent) {
    app.listen(3000);
    console.log('Express started on port 3000');
}
